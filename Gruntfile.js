module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        // Create the webfont files:
        webfont: {
                icons: {
                        src: '/tmp/icons/icons/actions/22@2x/*.svg',
                        dest: './dist',
                        options: {
                            stylesheet: 'scss',
                            types:  'eot,woff2,ttf,svg',
                            optimize: false
                            //normalize: true,
                            //ttfautohint: false,
                            //fontHeight: 1024
                            //engine: "node"
                            //relativeFontPath: "../../common/css/dist/"
                        }
                }
        }
    });
    grunt.loadNpmTasks('grunt-webfont');
    
    // Run the default task by executing "grunt" in the CLI:
    grunt.registerTask('default', ['webfont']);
};

