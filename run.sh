if [ ! -d "$DIRECTORY" ]
then
    mkdir -p /tmp/icons
    git clone https://anongit.kde.org/breeze-icons.git /tmp/icons
    cd /tmp/icons/icons/actions/
#     mkdir -p 512
else
    cd cd /tmp/icons/
    git pull
fi
# cd /tmp/icons/icons/actions/24
# 
# for file in *.svg; do
#     echo "../512/$file"
#     rsvg-convert "$file" -w 512 -h 512 -f svg -o ../512/${file%.svg}.svg
# done

cd /usr/src/app
mkdir -p dist
yarn add grunt-cli grunt grunt-webfont grunt-svg-modify svg-modify
./node_modules/grunt-cli/bin/grunt
# tail -f /dev/null
